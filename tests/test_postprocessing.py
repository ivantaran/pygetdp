#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Benjamin Vial
# License: MIT


from pygetdp import PostProcessing, render


def test_postprocessing():
    print("\n----------------\n")
    pp = PostProcessing()
    p0 = pp.add("test", "formulation")
    qt = p0.add()
    qt.add(Name="a", Type="Local", Value="{a}", In="Somewhere", Jacobian="Jac")
    qt.add(Name="b", Type="Integral", Value="{b}", In="Somewhere", Jacobian="Jac")
    #

    render(pp.code)
    ref = """PostProcessing{\n    { Name test; NameOfFormulation formulation; \n         Quantity {  \n            {  Name a; Value  {  Local {  [ {a} ]; In Somewhere; Jacobian Jac;  } } }\n            {  Name b; Value  {  Integral {  [ {b} ]; In Somewhere; Jacobian Jac;  } } }\n            }\n    }\n    }\n"""

    assert pp.code == ref
