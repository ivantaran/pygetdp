#!/usr/bin/python
# -*- coding: utf-8 -*-
#

import os

from pygetdp import render


def test_render():
    path_ = os.path.dirname(os.path.abspath(__file__))
    render_dir = os.path.join(path_, "rendered")
    try:
        os.mkdir(render_dir)
    except FileExistsError:
        pass
    # files=["mStrip", "Jacobian_Lib", "Integration_Lib", "EleSta_v", "test", "getdp"]
    files = ["getdp"]
    for f in files:
        render(
            os.path.join(path_, "src", "{}.pro".format(f)),
            os.path.join(render_dir, "{}.png".format(f)),
        )


if __name__ == "__main__":

    test_render()
