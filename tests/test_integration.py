#!/usr/bin/python
# -*- coding: utf-8 -*-
#


from pygetdp import Integration, render


def test_integration():
    print("\n----------------\n")

    integ = Integration()
    ii = integ.add("Int1", Criterion="jljdl")
    ic1 = ii.add()
    ic = ic1.add("Gauss")
    i = ic.add()
    i.add(GeoElement="Triangle", NumberOfPoints=4)
    i.add(GeoElement="Quadrangle ", NumberOfPoints=4)
    i.add(GeoElement="Tetrahedron", NumberOfPoints=4)
    i.add(GeoElement="Hexahedron ", NumberOfPoints=6)
    i.add(GeoElement="Prism", NumberOfPoints=9)

    render(integ.code)
    # for js, s in enumerate(["Vol", "Sur", "Lin"]):
    #     jac.add(Name=s)
    #     jac.items[js].add().add(Region="All", Jacobian=s)
    # ref = """Jacobian{\n    { Name Vol; \n         Case  {  \n           { Region All; Jacobian Vol; } \n         }\n    }\n    { Name Sur; \n         Case  {  \n           { Region All; Jacobian Sur; } \n         }\n    }\n    { Name Lin; \n         Case  {  \n           { Region All; Jacobian Lin; } \n         }\n    }\n    }\n"""
    #
    # assert jac.code == ref
