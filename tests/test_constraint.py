#!/usr/bin/python
# -*- coding: utf-8 -*-
#


from pygetdp import Constraint

# Constraint {
#   { Name DirichletBoundaryCondition1; Type Assign;
#     Case {
#       { Region Surf0; Value 0.; }
#       { Region Surf1; Value 1.; }
#     }
#   }
# }


# Constraint {
#   { Name ElectricalCircuit; Type Network;
#     Case Circuit1 {
#       { Region VoltageSource; Branch {1,2}; }
#       { Region PrimaryCoil; Branch {1,2}; }
#     }
#     Case Circuit2 {
#       { Region SecondaryCoil; Branch {1,2}; }
#       { Region Charge; Branch {1,2}; }
#     }
#   }
# }


def test_constraint():
    print("\n----------------\n")
    #
    # ci1 = CaseItem_("VoltageSource", Branch=[1, 2], comment="a case item")
    # print(ci1.code)
    # ci2 = CaseItem_("PrimaryCoil", Branch=[1, 2])
    # print(ci2.code)
    #
    # cse = Case_("Circuit1")
    # print(cse.code)
    # cse.add("VoltageSource", Branch=[1, 2])
    # print(cse.code)
    # cse.add("PrimaryCoil", Branch=[1, 2])
    # print(cse.code)
    #
    # cse = Case_()
    # cse.add(
    #     "SurfBlochRight",
    #     Type="LinkCplx",
    #     RegionRef="SurfBlochLeft",
    #     Coefficient="dephx[]",
    #     Function="Vector[$X-dx,$Y,$Z]",
    # )
    # cse.add(
    #     "SurfBlochTop",
    #     Type="LinkCplx",
    #     RegionRef="SurfBlochBot",
    #     Coefficient="dephy[]",
    #     Function="Vector[$X,$Y-dy,$Z]",
    # )
    #
    # # print(cse.code)
    # dirichlet = Constraint_(Name="Dirichlet", Type="Assign")
    # case0 = dirichlet.add("Testcase")
    #
    # case0.add("Surf0", Value=0)
    # case0.add("Surf1", Value=1)
    #
    # print(dirichlet.code)
    # case1 = dirichlet.add()
    # case1.add("Surf2", Value=2)
    # case1.add("Surf3", Value=3)
    # print(dirichlet.code)
    #
    # # Constraint {
    # #
    # # 		{Name Bloch;
    # # 		    Case {
    # #                     { Region SurfBlochRight; Type LinkCplx ; RegionRef SurfBlochLeft;
    # #                       Coefficient dephx[]; Function Vector[$X-dx,$Y,$Z] ;
    # #                     }
    # #                     { Region SurfBlochTop; Type LinkCplx ; RegionRef SurfBlochBot;
    # #                       Coefficient dephy[]; Function Vector[$X,$Y-dy,$Z] ;
    # #                     }
    # # 			 }
    # # 		}
    # # }
    #
    # bc = Constraint_(
    #     Name="Dirichlet", Type="Assign", comment="dirichlet boundary conditions"
    # )
    # bc.add("case1", comment="The first case")
    # bc.cases[0].add("Surf0", Value=0, comment="solution is 0 on Surf0")
    # bc.cases[0].add("Surf1", Value=1, comment="another specified value on the boundary")
    # bc.add("case2", comment="The second case")
    # bc.cases[1].add("Surf2", Value=2)
    # bc.cases[1].add("Surf3", Value=3)
    #
    # print(bc.code)

    const = Constraint()
    dirichlet = const.add(Name="Dirichlet", Type="Assign")
    case0 = dirichlet.add("case0")
    case0.add(Region="Surf0", Value=0)
    case0.add(Region="Surf1", Value=1)

    Dirichlet2 = const.assign(Name="Dirichlet2")
    case0 = Dirichlet2.add("case0")
    case0.add("Surf0", Value=0)
    case0.add("Surf1", Value=1)

    print(const.code)

    #
    # ref = """// --- A useful description ---\nGroup{\n    the contents of this object\n    This is some raw code\n    Another piece of code\n    // A short comment\n    /*  A long comment A long comment A long comment A long comment A long\n    comment A long comment A long comment A long comment A long comment A\n    long comment A long comment A long comment A long comment A long\n    comment A long comment A long comment A long comment A long comment A\n    long comment A long comment A long comment A long comment A long\n    comment A long comment A long comment A long comment A long comment A\n    long comment A long comment A long comment */\n    }\n"""
    # assert test.code == ref
