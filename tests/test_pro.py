#!/usr/bin/python
# -*- coding: utf-8 -*-
#


import pygetdp
from pygetdp import render


def test_pro():
    pro = pygetdp.Problem()

    pro.add_comment("THIS IS AN EXAMPLE OF A PRO FILE " * 10)
    pro.include("some_file.txt")

    pro.add_raw_code('\nInclude "params.dat"; \n')

    # groups
    pro.group.define(id=["Air", "Core"])
    air = pro.group.add(id="Air", glist=[1000])
    pro.group.Region(id="Air2", glist=[1001], comment="Air region")
    pro.group.Region("Air3", 1002)
    core = pro.group.Region("Core", [2000, 3000])
    pro.group.Region("Inductor")
    pro.group.Region("NonConductingDomain", [air, core])

    # functions
    function = pygetdp.Function()
    function.define(name=["epsilon", "mu"])
    function.constant(name="kx", expression="3", comment="constant")
    function.add(name="e", expression=3.22, region="air")
    function.add("f", "kx + 1")
    function.add(name="rho", expression="x^2+y^2 + kx", arguments=["x", "y"])
    function.add(
        "g",
        expression="3 * length^3 - 1 / width^2 + Sin[$X]",
        arguments=["length", "width"],
        region=[air, core],
        comment="some function defined in a sub domain",
    )

    regions = ["air", "core"]
    epsi = [1, 13]

    for r, value in zip(regions, epsi):
        function.add("epsilon", value, r)

    z0 = function.Complex(2.1, -4.7)
    function.constant("z0", z0)
    function.constant("z1", 12 - 3 * 1j)
    Id = function.TensorDiag(1, 1, 1)
    for r, value in zip(regions, epsi):
        function.add("epsilon_tens", region=r, expression=str(value) + " * " + Id)

    params = {}
    params["a"] = 1 + 1j
    params["b"] = "Cos[1.2]", "a getdp expression"
    params["c"] = 3.32
    params["d"] = 12, "description"

    function.add_params(params)

    pro.function = function

    pro.filename = "./tests/src/test_pro.pro"
    pro.make_file()
    code = pro.get_code()
    print(code)
    pro.write_file()
    render(pro.filename, "./tests/rendered/test_pro.png")
