#!/usr/bin/python
# -*- coding: utf-8 -*-
#


import os

from pygetdp import *


def test_pro_getdp():
    pro = Problem()
    comment = """
    File 'mStrip.pro'

    This file defines the problem dependent data structures for the
    microstrip problem.

    To compute the solution:
        getdp mStrip -solve EleSta_v

    To compute post-results:
        getdp mStrip -pos Map
        or getdp mStrip -pos Cut"""

    pro.add_comment(str(comment))

    ## -- group --

    Air = pro.group.add("Air", 101)
    Diel1 = pro.group.add("Diel1", 111)
    Ground = pro.group.add("Ground", 120)
    Line = pro.group.add("Line", 121)
    SurfInf = pro.group.add("SurfInf", 130)
    DomainCC_Ele = pro.group.add("DomainCC_Ele", [Air, Diel1])
    DirichletSurf = pro.group.add("DirichletSurf", [Ground, SurfInf])
    # pro.group.define("DomainInf")

    ## -- function --

    eps0 = 8.854187818e-12
    e = 1.0e-7

    function = pro.function
    epsr = "epsr"
    function.add(name=epsr, expression=1.0, region=Air)
    function.add(name=epsr, expression=9.8, region=Diel1)
    function.constant("eps0", eps0)
    function.constant("e", e)

    ## -- constraint --

    const = pro.constraint
    dirichlet = const.add(Name="ElectricScalarPotential", Type="Assign")
    case0 = dirichlet.add()
    case0.add(Region=DirichletSurf, Value=0)
    case0.add(Region=Line, Value=1e-3)

    ## -- functionspace --

    fs = pro.functionspace
    Hgrad_v_Ele = fs.add("Hgrad_v_Ele", Type="Form0")
    Hgrad_v_Ele.add_basis_function(
        Name="sn",
        NameOfCoef="vn",
        Function="BF_Node",
        Support="DomainCC_Ele",
        Entity="NodesOf[All]",
    )
    Hgrad_v_Ele.add_constraint(
        NameOfCoef="vn",
        EntityType="NodesOf",
        NameOfConstraint="ElectricScalarPotential",
    )

    ## -- jacobian --
    # Val_Rint = 200.0e-3
    # Val_Rext = 250.0e-3

    jac = pro.jacobian
    jvol = jac.add(Name="Vol")
    jcase = jvol.add()
    # jcase.add(Region="DomainInf", Jacobian=jac.VolSphShell(Val_Rint, Val_Rext))
    jcase.add(Region="All", Jacobian="Vol")

    ## -- integration --
    integ = pro.integration
    GradGrad = integ.add("GradGrad")
    ic1 = GradGrad.add()
    ic = ic1.add("Gauss")
    i = ic.add()
    geomels = ["Triangle", "Quadrangle", "Tetrahedron", "Hexahedron", "Prism"]
    numels = [4, 4, 4, 6, 9]
    for g, n in zip(geomels, numels):
        i.add(GeoElement=g, NumberOfPoints=n)

    ## -- formulation --
    formulation = pro.formulation
    Electrostatics_v = formulation.add(Name="Electrostatics_v", Type="FemEquation")
    q = Electrostatics_v.add_quantity()
    q.add(Name="v", Type="Local", NameOfSpace="Hgrad_v_Ele")
    e = Electrostatics_v.add_equation()
    e.add(
        "Galerkin",
        "epsr[] * Dof{d v} , {d v}",
        In="DomainCC_Ele",
        Jacobian="Vol",
        Integration="GradGrad",
    )

    ## -- resolution --
    resolution = pro.resolution
    EleSta_v = resolution.add(Name="EleSta_v")
    s = EleSta_v.add_system()
    s.add(Name="Sys_Ele", NameOfFormulation="Electrostatics_v")
    op = EleSta_v.add_operation()
    op.Generate("Sys_Ele")
    op.Solve("Sys_Ele")
    op.SaveSolution("Sys_Ele")

    ## -- postprocessing --
    pp = pro.postprocessing
    EleSta_v = pp.add("EleSta_v", NameOfFormulation="Electrostatics_v")
    qt = EleSta_v.add()
    qt.add(Name="v", Type="Local", Value="{v}", In="DomainCC_Ele", Jacobian="Vol")
    qt.add(Name="e", Type="Local", Value="-{d v}", In="DomainCC_Ele", Jacobian="Vol")
    qt.add(
        Name="d",
        Type="Local",
        Value="-eps0*epsr[] * {d v}",
        In="DomainCC_Ele",
        Jacobian="Vol",
    )

    ## -- postoperation --
    postoperation = pro.postoperation
    Map = postoperation.add(Name="Map", NameOfPostProcessing="EleSta_v")
    op = Map.add()
    op.add("v", OnElementsOf="DomainCC_Ele", File="mStrip_v.pos")
    op.add("e", OnElementsOf="DomainCC_Ele", File="mStrip_e.pos")
    Cut = postoperation.add(Name="Cut", NameOfPostProcessing="EleSta_v")
    op = Cut.add()
    op.add("e", OnLine="{{e,e,0}{10.e-3,e,0}} {500}", File="Cut_e")

    pro.filename = "./tests/src/mStrip.pro"
    pro.make_file()
    pro.write_file()
    code = pro.get_code()
    render(code)

    geom_filename = "./tests/src/mStrip.geo"

    os.system("gmsh -v 4 {} -2".format(geom_filename))
    os.system("getdp -v 4 {} -solve EleSta_v".format(pro.filename))
    os.system("getdp -v 4 {} -pos Map".format(pro.filename))
    os.system("getdp -v 4 {} -pos Cut".format(pro.filename))
    #
    # render(pro.filename, "./tests/rendered/test_pro.png")
