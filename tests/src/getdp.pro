FunctionSpace {
  { Name H1; Type Form0;
    BasisFunction {
      { Name sn; NameOfCoef vn; Function BF_Node; Support D; Entity NodesOf[All]; }
    }
  }
}
Formulation{
  { Name Poisson; Type FemEquation;
    Quantity {
      { Name v; Type Local; NameOfSpace H1; }
    }
    Equation {
      Integral { [ a[] * Dof{d v}, {d v} ] ; In D; Jacobian V; Integration I; }
      Integral { [ f[], {v} ] ; In D; Jacobian V; Integration I; }
    }
  }
}
