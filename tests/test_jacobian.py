#!/usr/bin/python
# -*- coding: utf-8 -*-
#


from pygetdp import Jacobian, render


def test_jacobian():
    print("\n----------------\n")
    jac = Jacobian()
    for js, s in enumerate(["Vol", "Sur", "Lin"]):
        jac.add(Name=s)
        jac.items[js].add().add(Region="All", Jacobian=s)
    render(jac.code)

    ref = """Jacobian{\n    { Name Vol; \n         Case  {  \n           { Region All; Jacobian Vol; } \n         }\n    }\n    { Name Sur; \n         Case  {  \n           { Region All; Jacobian Sur; } \n         }\n    }\n    { Name Lin; \n         Case  {  \n           { Region All; Jacobian Lin; } \n         }\n    }\n    }\n"""

    assert jac.code == ref

    jac = Jacobian()
    for js, s in enumerate(["Vol", "Sur", "Lin"]):
        jac.add(Name=s)
        jac.add_case(item_number=js)
        jac.add_case_item(item_number=js, case_number=0, Region="All", Jacobian=s)

    assert jac.code == ref
