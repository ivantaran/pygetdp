#!/usr/bin/python
# -*- coding: utf-8 -*-
#


from pygetdp import Formulation, render


def test_jacobian():

    formulation = Formulation()
    elstat_formu = formulation.add(Name="Electrostatics_v", Type="FemEquation")
    q = elstat_formu.add_quantity()
    q.add(Name="v", Type="Local", NameOfSpace="Hgrad_v")
    q.add(Name="w", Type="Local", NameOfSpace="Hgrad_w")
    e = elstat_formu.add_equation()
    e.add(
        "Integral",
        "epsr[] * Dof{Grad v} , {Grad v}",
        In="Domain",
        Jacobian="Vol",
        Integration="Int_1",
    )
    e.add("GlobalTerm", "- Dof{Q}/eps0 , {V}", In="SkinDomainC")

    render(formulation.code)
    # ref = """Jacobian{\n    { Name Vol; \n         Case  {  \n           { Region All; Jacobian Vol; } \n         }\n    }\n    { Name Sur; \n         Case  {  \n           { Region All; Jacobian Sur; } \n         }\n    }\n    { Name Lin; \n         Case  {  \n           { Region All; Jacobian Lin; } \n         }\n    }\n    }\n"""

    # assert jac.code == ref
