#!/usr/bin/python
# -*- coding: utf-8 -*-
#


from pygetdp.getdp_object import GetDPObject


def test_object():
    test = GetDPObject(
        name="Group",
        content="the contents of this object",
        comment="--- A useful description ---",
    )
    print(test.code)

    ref = "// --- A useful description ---\nGroup{\n    the contents of this object\n    }\n"
    assert test.code == ref

    test.add_raw_code("This is some raw code")
    # print(test.code)
    test.add_raw_code("Another piece of code")
    # print(test.code)
    test.add_comment("A short comment")
    # print(test.code)
    test.add_comment("A long comment " * 30)
    print(test.code)

    ref = """// --- A useful description ---\nGroup{\n    the contents of this object\n    This is some raw code\n    Another piece of code\n    // A short comment\n    /*  A long comment A long comment A long comment A long comment A long\n    comment A long comment A long comment A long comment A long comment A\n    long comment A long comment A long comment A long comment A long\n    comment A long comment A long comment A long comment A long comment A\n    long comment A long comment A long comment A long comment A long\n    comment A long comment A long comment A long comment A long comment A\n    long comment A long comment A long comment */\n    }\n"""
    assert test.code == ref
