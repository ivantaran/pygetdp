# -*- coding: utf-8 -*-
"""
FunctionSpace
===============

An example to define a FunctionSpace object
"""

from pygetdp import FunctionSpace
from pygetdp.helpers import build_example_png, print_html

##############################################################################
# Nodal finite element spaces
# -----------------------------
#
# The most elementary function space is the nodal finite element space,
# defined on a mesh of a domain :math:`W` and denoted :math:`S_0(W)` (associated finite elements
# can be of various geometries), and associated with essential boundary
# conditions (Dirichlet conditions). It contains 0-forms, i.e.,
# scalar fields of potential type:
#
# .. math::
#    v = \sum_{n \in N}  v_n \, s_n, \quad v \in S_0(W)
#
# where :math:`N` is the set of nodes of :math:`W`, :math:`s_n` is the
# nodal basis function associated with node :math:`n`
# and :math:`v_n` is the value of :math:`v` at node :math:`n`. It is defined by
#

fs = FunctionSpace()
hgrad = fs.add("Hgrad", Type="Form0")

hgrad.add_basis_function(
    Name="sn",
    NameOfCoef="vn",
    Function="BF_Node",
    Support="Domain",
    Entity="NodesOf[All]",
)

hgrad.add_constraint(
    NameOfCoef="vn",
    EntityType="NodesOf",
    NameOfConstraint="DirichletBoundaryCondition1",
)
print_html(fs.code, tmp_file="out0.html")


####################################
# .. raw:: html
#     :file: out0.html


####################################
# Function ``sn`` is the built-in basis function ``BF_Node`` associated with all nodes
# (``NodesOf``) in the mesh of ``W (Domain)``. Previously defined Constraint
# ``DirichletBoundaryCondition1`` (see ``Constraint`` examples) is used as boundary condition.
#
# In the example above, ``Entity NodesOf[All]`` is preferred to ``Entity
# NodesOf[Domain]``. In this way, the list of all the nodes of ``Domain``
# will not have to be generated. All the nodes of each geometrical element in
# ``Support Domain`` will be directly taken into account.


##############################################################################
# High order nodal finite element spaces
# ---------------------------------------
# Higher order finite elements can be directly taken into account by ``BF_Node``.
# Hierarchical finite elements for 0-forms can be used by simply adding other
# basis functions (associated with other geometrical entities, e.g., edges
# and facets) to ``BasisFunction``, e.g.,

hgrad.add_basis_function(
    Name="s2",
    NameOfCoef="v2",
    Function="BF_Node_2E",
    Support="Domain",
    Entity="EdgesOf[All]",
)
hgrad.add_constraint(
    NameOfCoef="v2",
    EntityType="EdgesOf",
    NameOfConstraint="DirichletBoundaryCondition1",
)
print_html(fs.code, tmp_file="out1.html")

####################################
# .. raw:: html
#     :file: out1.html


##############################################################################
# Nodal finite element space with floating potentials
# -----------------------------------------------------
# A scalar potential with floating values :math:`v_f` on certain boundaries
# :math:`G_f`, :math:`f \in C_f`,
# e.g., for electrostatic problems, can be expressed as
#
# .. math::
#    v = \sum_{n \in N_v}  v_n \, s_n + \sum_{f \in N_f}  v_f \, s_f, \quad v \in S_0(W)
#
# where :math:`N_v` is the set of inner nodes of :math:`W` and each function :math:`s_f` is associated
# with the group of nodes of boundary :math:`G_f`, :math:`f \in C_f` (``SkinDomainC``); :math:`s_f` is the
# sum of the nodal basis functions of all the nodes of :math:`C_f`. Its function space
# is defined by

fs = FunctionSpace()
Hgrad_v_floating = fs.add("Hgrad_v_floating", Type="Form0")


Hgrad_v_floating.add_basis_function(
    Name="sn",
    NameOfCoef="vn",
    Function="BF_Node",
    Support="Domain",
    Entity="NodesOf[All, Not SkinDomainC]",
)
Hgrad_v_floating.add_basis_function(
    Name="sf",
    NameOfCoef="vf",
    Function="BF_GroupOfNodes",
    Support="Domain",
    Entity="GroupsOfNodesOf[SkinDomainC]",
)
Hgrad_v_floating.add_global_quantity(
    Name="GlobalElectricPotential", Type="AliasOf", NameOfCoef="vf"
)
Hgrad_v_floating.add_global_quantity(
    Name="GlobalElectricCharge", Type="AssociatedWith", NameOfCoef="vf"
)

Hgrad_v_floating.add_constraint()
print_html(fs.code, tmp_file="out2.html")

####################################
# .. raw:: html
#     :file: out2.html


##############################################################################
# Edge finite element space
# -----------------------------------------------------
# Another space is the edge finite element space, denoted :math:`S_1(W)`,
# containing 1-forms, i.e., curl-conform fields:
#
# .. math::
#    h = \sum_{e \in E}  h_e \, s_e, \quad h \in S_1(W)
#
# where :math:`E` is the set of edges of :math:`W`, :math:`s_e` is the edge basis function for edge
# :math:`e` and he is the circulation of :math:`h` along edge :math:`e`. It is defined by
#

fs = FunctionSpace()
Hcurl_h = fs.add("Hcurl_h", Type="Form1")

Hcurl_h.add_basis_function(
    Name="se",
    NameOfCoef="he",
    Function="BF_Edge",
    Support="Domain",
    Entity="EdgesOf[All]",
)

Hcurl_h.add_constraint(
    NameOfCoef="he",
    EntityType="EdgesOf",
    NameOfConstraint="DirichletBoundaryCondition1",
)
print_html(fs.code, tmp_file="out3.html")

####################################
# .. raw:: html
#     :file: out3.html

##############################################################################
# Finally we write and render the code as png

build_example_png(fs.code)
