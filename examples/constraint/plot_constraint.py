# -*- coding: utf-8 -*-
"""
Constraint
===============

An example to define a Constraint object
"""

from pygetdp import Constraint
from pygetdp.helpers import build_example_png, print_html

##############################################################################
# Constraints are referred to in ``FunctionSpaces`` and are usually used for boundary
# conditions (``Assign`` type). For example, essential conditions on two surface
# regions, ``Surf0`` and ``Surf1``, will be first defined by

const = Constraint()
dirichlet = const.add(Name="DirichletBoundaryCondition1", Type="Assign")
case0 = dirichlet.add()
case0.add(Region="Surf0", Value=0)
case0.add(Region="Surf1", Value=1)
case0.add(Region="Surf2", Value=1, TimeFunction="3*TimeFct_Exp[]")

print_html(const.code, tmp_file="out0.html")

##############################################################################
# The way the ``Values`` are associated with ``Regions`` (with their nodes, their edges,
# their global regions, …) is defined in the ``FunctionSpaces`` which use the
# ``Constraint``. In other words, a ``Constraint`` defines data but does not define
# the method to process them. A time dependent essential boundary condition on
# ``Surf2`` is introduced (cf. ``Function`` examples for the definition of
# ``TimeFct_Exp[]``).It is important to notice that the time
# dependence cannot be introduced in the Value field,
# since the Value is only evaluated once during the pre-processing.


####################################
# .. raw:: html
#     :file: out0.html


####################################
# Other constraints can be referred to in ``Formulations``.
# It is the case of those defining electrical circuit connections (``Network`` type), e.g.,


const = Constraint()
dirichlet = const.add(Name="ElectricalCircuit", Type="Network")
Circuit1 = dirichlet.add("Circuit1")
Circuit1.add(Region="VoltageSource", Branch=[1, 2])
Circuit1.add(Region="PrimaryCoil", Branch=[1, 2])

Circuit2 = dirichlet.add("Circuit2")
Circuit2.add(Region="SecondaryCoil", Branch=[1, 2])
Circuit2.add(Region="Charge", Branch=[1, 2])

print_html(const.code, tmp_file="out1.html")

####################################
# .. raw:: html
#     :file: out1.html


####################################
# which defines two non-connected circuits (``Circuit1`` and ``Circuit2``),
# with an independent numbering of nodes: region ``VoltageSource`` is
# connected in parallel with region ``PrimaryCoil``, and region
# ``SecondaryCoil`` is connected in parallel with region ``Charge``.


##############################################################################
# Finally we write and render the code as png

build_example_png(const.code)
