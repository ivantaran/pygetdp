# -*- coding: utf-8 -*-
"""
Jacobian
==========================================

An example to define a Jacobian object
"""

from pygetdp import Jacobian
from pygetdp.helpers import build_example_png, print_html

##############################################################################
# Instanciate the :py:class:`Jacobian` object

jac = Jacobian()
##############################################################################
# Then we define the different Jacobians as follow:

for js, s in enumerate(["Vol", "Sur", "Lin"]):
    jac.add(Name=s)  # add a new Jacobian item
    jac.items[js].add()  # add a new case
    # add a new item in this case with Region and Jacobian type
    jac.items[js].cases[0].add(Region="All", Jacobian=s)

print_html(jac.code)

####################################
# .. raw:: html
#     :file: out.html

##############################################################################
# Finally we write and render the code as png

build_example_png(jac.code)
